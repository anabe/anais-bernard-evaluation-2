// récupération du contexte pour dessiner le canvas du haut
const topCirclesCanvas = document.querySelector('#top-circles');
const topCirclesContext = topCirclesCanvas.getContext('2d');
// Pour dessiner dans le canavs du bas
const bottomCirclesCanvas = document.querySelector('#bottom-circles');
const bottomCirclesContext = bottomCirclesCanvas.getContext('2d');
// Garde en mémoire les variations sur l'emplacement des dessins effectués
// ainsi on peut le faire bouger
// Faire en sorte que les variations x et y soient comprises entre -50px et 50px
const variations = { x: 0, y: 0 };
// Valeur de variation à chaque mouvement
const variationsIntensity = 1;

// Définit si le mouvement est vers le haut ou le bas
let bouncingTop = true;
// Définit la position actuelle du rebond
let bouncingValue = 0;

// renvoie un nombre aléatoire pour le déplacement aléatoire (les vibrations des cercles sur le rendu)
// ça renvoie un nombre entre 0 et max
// Le max = 100 de l'argument de fonction permet de lui attribuer une valeur par défaut
function getRandomNumber(max = 100) {
  return (Math.floor(Math.random() * max));
}

// Dessine les cercles du canvas du haut en Desktop
function drawDesktopTopCircles() {
  // Récupère la largeur de l'écran
  const width = document.documentElement.clientWidth;
  // Définit la taille du canvas (largeur et hauteur)
  // Cela permet également de réinitialiser le canvas, tous les dessins réalisés seront effacés
  topCirclesCanvas.width = width;
  topCirclesCanvas.height = 800;
  // Les valeurs x et y aux quels placés les cercles
  // Attribution de la valeur de variation
  const beginX = (0.6 * width) + variations.x;
  const beginY = variations.y;

  // cercle noir
  // On commence à dessiner
  topCirclesContext.beginPath();
  // Définit la couleur de remplissage
  topCirclesContext.fillStyle = '#000000';
  // Définit son tracé par rapport à x,y, son rayon, son angle et son orientation
  // Utilise width pour sa position et sa taille pour être proportionnel aux dimensions de la page
  topCirclesContext.arc(beginX + width / 12.5, beginY + (width / 12.5), width / 12.5, 0, 2 * Math.PI, false);
  // Remplit
  topCirclesContext.fill();

// Les 2 cercles suivants reprennent le même principe que précédemment, avec des valeurs différentes

  // Cercle rose
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#EEBB9E';
  topCirclesContext.arc(beginX + width / 6.5, beginY + (width / 8), width / 11.5, 0.25 * Math.PI, Math.PI, true);
  topCirclesContext.fill();


  // Cercle orange
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#E93B00';
  topCirclesContext.arc(beginX + width / 12.5, beginY + (width / 6.5), width / 13.5, 0, Math.PI * 2, false);
  topCirclesContext.fill();

// Pour l'ovale : utilise ellipe au lieu de arc avec comme paramètre sa position x-y, son rayon en x-y, son angle et son orientation
  // Ovale jaune
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#FCA929';
  topCirclesContext.ellipse(beginX + width / 6, beginY + (width / 6), width / 11.5, width / 20, 0, 2 * Math.PI, false);
  topCirclesContext.fill();

  
  // Ajout d'un dernier cercle qui rebondit.
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#860B68';
  // Réduit la position y de bouncingValue
  topCirclesContext.arc(110, 276 - bouncingValue, 100, 0, 2 * Math.PI, false);
  topCirclesContext.fill();
}

// Pour les 3 fonctions suivantes, le fonctionnement reste le même. Seules les valeurs changent (donc voir les commentaires de la fonction précedante)

// Dessine les cercles du canvas du bas en Desktop
function drawDesktopBottomCircles() {
  const width = document.documentElement.clientWidth
  bottomCirclesCanvas.width = width;
  bottomCirclesCanvas.height = 500;
  const beginX = (width * 0.6) + variations.x;
  const beginY = variations.y;

  // black circle
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = 'black';
  bottomCirclesContext.arc(width / 12.5, width / 12.5, width / 12.5, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();

  // yellow circle
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = '#FCA929';
  bottomCirclesContext.arc(width / 40, width / 7.5, width / 17, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();

  // pink circle
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = '#EEBB9E';
  bottomCirclesContext.arc(width / 12, width / 5, width / 11, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();

  // orange ellipse (right part of canvas)
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = '#E93B00';
  bottomCirclesContext.ellipse(beginX + (width / 12.5), beginY + (width / 6), width / 9, width / 16.5, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();
}

// Dessine les cercles du canvas du haut en responsive (notamment téléphone)
function drawPhoneTopCircles() {
  const width = document.documentElement.clientWidth;
  topCirclesCanvas.width = width;
  topCirclesCanvas.height = 500;

  // pink circle
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#EEBB9E';
  topCirclesContext.arc(5, 200, width / 4.5, 0, 2 * Math.PI, false);
  topCirclesContext.fill();

  // yellow ellipse
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#FCA929';
  topCirclesContext.ellipse(width - 50, 240, width / 3.5, width / 5.5, 0, 2 * Math.PI, false);
  topCirclesContext.fill();

  // orange circle
  topCirclesContext.beginPath();
  topCirclesContext.fillStyle = '#E93B00';
  topCirclesContext.arc(width - 50, 190, width / 5, 0, Math.PI * 2, false);
  topCirclesContext.fill();
}

// Dessine les cercles du canvas du bas en responsive
function drawPhoneBottomCircles() {
  const width = document.documentElement.clientWidth;
  bottomCirclesCanvas.width = width
  bottomCirclesCanvas.height = 800;

  // yellow circle
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = '#FCA929';
  bottomCirclesContext.arc(10, 400, width / 5, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();

  // orange ellipse
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = '#E93B00';
  bottomCirclesContext.ellipse(50, 450, width / 3.5, width / 5.5, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();

  // pink circle
  bottomCirclesContext.beginPath();
  bottomCirclesContext.fillStyle = '#EEBB9E';
  bottomCirclesContext.arc(width - 30, 350, width / 6, 0, 2 * Math.PI, false);
  bottomCirclesContext.fill();
}

// Fonction définissant la nouvelle valeur de variation y si le mouvement est vers le haut
function moveUp() {
  // Si la variation y n'est pas déjà de 50px
  if (variations.y > -50) {
    // On réduit la variation y de variation intensity pour l'approcher de -50
    variations.y -= variationsIntensity;
  } else {
    // Sinon on l'éloigne de la valeur -50
    variations.y += variationsIntensity;
  }
}

// Les 3 prochaines fonctions utilisent le même principe, le seule changement est la variation bas ou gauche droite.

// Fonction définissant la nouvelle valeur de variation y si le mouvement est vers le bas
function moveDown() {
  if (variations.y < 50) {
    variations.y += variationsIntensity;
  } else {
    variations.y -= variationsIntensity;
  }
}

// Fonction définissant la nouvelle valeur de variation x si le mouvement est vers la droite
function moveRight() {
  if (variations.x < 50) {
    variations.x += variationsIntensity;
  } else {
    variations.x -= variationsIntensity;
  }
}

// Fonction définissant la nouvelle valeur de variation x si le mouvement est vers la gauche
function moveLeft() {
  if (variations.x > -50) {
    variations.x -= variationsIntensity;
  } else {
    variations.x += variationsIntensity;
  }
}

// Permet d'attribuer une nouvelle valeur à la variation, permet de faire bouger les cercles
function moveCircle() {

  // Prend un chiffre au hasard compris entre 0 et 8. Le 1 + permet d'avoir un chiffre en 1 et 9 compris
  const movement = 1 + getRandomNumber(9);

  // 1 : down move + left move
  // 2 : down move
  // 3 : down move + right move
  // 4 : left move
  // 5 : no move
  // 6 : right move
  // 7 : up move + left move
  // 8 : up move
  // 9 : up move + right move

  // Switch : peremt de définir des actions en fonction de la valeur de mouvement
  switch (movement) {

    // Si le mouvement = 1 alors on le bouge en bas à gauche
    case 1: moveDown();
      moveLeft();
      break;

    case 2: moveDown();
      break;

    case 3: moveDown();
      moveRight();
      break;

    case 4: moveLeft();
      break;

    case 5:
      break;

    case 6: moveRight();
      break;

    case 7: moveUp();
      moveLeft();
      break;

    case 8: moveUp();
      break;

    case 9: moveUp();
      moveRight();
      break;

    default:
      break;
  }
}

// Permet de modifier la valeur de bouncingValue pour définir le rebond du cercle
function bounceCircle() {

  // Si le rebond est vers le haut
  if(bouncingTop) {
    // Ajoute 2.5 fois la variationsIntensity
    bouncingValue += 2.5 * variationsIntensity;
  } else {
    // Sinon réduit de 2.5 fois la variationsIntensity
    bouncingValue -= 2.5 * variationsIntensity;
  }

  // Si le rebond est vers le haut et que la valeur de rebond est supérieur ou égale à 130 px
  if(bouncingTop && bouncingValue >= 130) {
    // Alors définit le rebond vers le bas (ie : pas un rebond vers le haut)
    bouncingTop = false;
  }

  // Si le rebond est vers le bas (ie : non vers le haut) et que la valeur de rebond est inférieur ou égale à 0 px
  if(!bouncingTop && bouncingValue <= 0) {
    // Alors on définis le rebond vers le haut
    bouncingTop = true;
  }
}

// Dessine tous les cercles des deux canvas
function drawCircles() {
  // Si la largeur de la page est supérieur à 750px
  if (document.documentElement.clientWidth > 750) {
    // Alors on dessine les cercles en version desktop
    drawDesktopTopCircles();
    drawDesktopBottomCircles();
  } else {
    // Sinon on dessine les cercles en version mobile
    drawPhoneTopCircles();
    drawPhoneBottomCircles();
  }
}

// On y met une fonction et un temps. Réalise la fonction tous les xtemps
// Toutes les 10milisecondes les cercles bougent et se redessinent
// Cet interval permet aussi d'avoir une version responsive qui vérifie la taille des écrans
setInterval(() => {
  moveCircle();
  bounceCircle();
  drawCircles();
}, 10);

// On appelle la fonction pour dessiner les cercles initalement
drawCircles();

